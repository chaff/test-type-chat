#include <stdio.h>

int main(int argc, char** argv)
{
    if (argc > 1){
        fprintf(stderr, "Why so argumentative?\n");
        return -1;
    }
    
    printf("Hello!\n");
    return 0;
    
}