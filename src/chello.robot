*** Settings ***
Library     Process

*** Variables ***
${python} =  python3
${sut} =  ./hello
${greeting} =  Hello!
${error} =  Why so argumentative?

*** Keywords ***
I have a bash shell with python3
    Set Global Variable  ${python}  python3
    Log  I have a shell with python 3

I run the utility without arguments
    [Arguments]  ${sut}
    ${result} =  Run Process  ${sut}
    Set Global Variable  ${result}
    Log  ${result.stdout}
    Log  ${result.stderr}

I run the utility with arguments
    [Arguments]  ${sut}  ${args}
    ${result} =  Run Process  ${sut}  ${args}
    Set Global Variable  ${result}
    Log  ${result.stdout}
    Log  ${result.stderr}

I should see my greeting in STDOUT
    [Arguments]  ${greeting}
    Should match  ${result.stdout}  ${greeting}  "Didn't say Hello!!!"

I should see an error in STDERR
    [Arguments]  ${error} 
    Should match  ${result.stderr}  ${error}  "Error Message Missing or Incorrect for argument usage!!!"


*** Test Cases ***

TTC_1.1 - Good path - No Arguments
    Given I have a bash shell with python3 
    When I run the utility without arguments  ${sut} 
    Then I should see my greeting in STDOUT  ${greeting}

TTC_1.2.1 - Proper Error - One Argument
    Given I have a bash shell with python3 
    When I run the utility with arguments  ${sut}  Arg1
    Then I should see an error in STDERR  ${error} 

TTC_1.2.2 - Proper Error - Two Arguments
    Given I have a bash shell with python3 
    When I run the utility with arguments  ${sut}  'Arg1 Arg2'
    Then I should see an error in STDERR  ${error} 
