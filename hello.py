#!/usr/bin/env python

"""
Test Type Chat

"""

import sys


def main() -> int:
    if len(sys.argv) == 1:
        say_hello()
        return 0
    else:
        print('Why so argumentative?', file=sys.stderr)
        return -1


def say_hello() -> None:
    """Prints a hello to stdout
    """
    print('Hello!')


if __name__ == '__main__':
    main()
