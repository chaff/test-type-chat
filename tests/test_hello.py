import sys
import hello

"""
Importing and calling into hello.py means this is "white-box"
testing, representing behaviors and expectations internal to
the development team

"""


def test_main_no_args(monkeypatch):
    monkeypatch.setattr("sys.argv", ["pytest"])
    assert hello.main() == 0


def test_main_args(monkeypatch):
    # Single arg test
    monkeypatch.setattr("sys.argv", ["pytest", "arg1"])
    assert hello.main() == -1

    # Multi-arg test
    monkeypatch.setattr("sys.argv", ["pytest", "arg1", "arg2"])
    assert hello.main() == -1


def test_say_hello(capsys):
    res = hello.say_hello()
    out, err = capsys.readouterr()
    sys.stdout.write(out)
    sys.stderr.write(err)
    assert res is None
    assert out.startswith('Hello!')
