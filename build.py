#!/usr/bin/env python

"""

Script to orchestrate cleaning, building, analyzing, testing, etc.

"""

import sys
import os


# Print help function
def help():
    print('build.py [command]')
    print('[command] is one of the following limited options:')
    print('commands:')
    print(' help')
    print('     print(this help')
    print(' clean')
    print('     clean report and cache states')
    print(' test')
    print('     run all tests ')
    print(' lint')
    print('     only run lint tests')
    print(' unit')
    print('     only run unit tests')
    print(' functional')
    print('     only run functional tests')


# Cleanup
def clean() -> bool:
    print('Cleaning')
    return True


# Linting
def lint() -> bool:
    print('\n>>>>>>>>>>>>  Linting...  <<<<<<<<<<<<<')
    os.system('pytest --flake8 --cache-clear hello.py tests/*.py')
    return True


# Unit Testing
def unit() -> bool:
    print('\n>>>>>>>>>>  Unit Testing...  <<<<<<<<<<')
    os.system('pytest --junitxml=reports/pytest-report.xml --cov -rfs')
    os.system('coverage report')
    os.system('coverage xml')
    return True


# Functional Testing
def functional() -> bool:
    print('\n>>>>>>>  Functional Testing...  <<<<<<<')
    os.system('python -m robot --outputdir reports tests')
    return True


# Program main:
def main(argv):
    """
    Main pipeline switchboard

    Args:
        argv (List): Arguments (stripped of process name)
    """

    # Parse options
    if argv == []:
        help()
        exit()
    for arg in argv:
        if arg == 'help':
            help()
            exit()
        elif arg == 'clean':
            clean()
        elif arg == 'test':
            lint()
            unit()
            functional()
        elif arg == 'lint':
            lint()
        elif arg == 'unit':
            unit()
        elif arg == 'functional':
            functional()
        else:
            print(f'ERROR: {arg=} is not a valid command')
            help()
            exit()


# Entry point.
if __name__ == "__main__":
    main(sys.argv[1:])
