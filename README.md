# Test Type Example - R&R Session

Super simple python app to help distinguish between test types

_The only purpose for this terrible example of coding is to talk through the different purposes and types of testing in a CI/CD pipeline_

## Initial Requirements

1. I need a command line utility that prints "Hello" to standard out when executed

## Refined Requirements

| CNF Tie | Story | AC | AT |
| --- | ------ |----------------- | ------------- |
| 1 | TTC-1: As a user, I want to see "Hello"<br>(without quotes) on standard<br>out when I run the utility<br>from bash without arguements  | AC1.1: <br> Given a bash shell with python3 <br> When I run the utility without arguments<br>Then "Hello" will be printed to STDOUT | TTC_1.1 |
|   |   | AC1.2: <br> Given a bash shell with python3 <br> When I run the utility with arguments<br>Then "Why so argumentative?" will be printed to STDERR | TTC_1.2.1<br>TTC_1_1.2.2 |

## Limitations
1. As stated above, the purpose of this toy example was to talk through types testing and illustrate how/where to incorporate them
1. The **python** version is fairly well put together: 
    1. `build.py` used for local and CI activities (though local is not using the pipeline docker container at the moment...)
    1. Linting, Unit, and Functional tests included
    1. Reports for Functional Tests sent to Gitlab Pages for immediate visibility of report artifacts
    1. Detail in re-using the same robot tests for the python script as the c binary:
        - Created a 755-chmod'd symbolic link `hello` to `hello.py` so the robot file could remain 100% the same, calling `hello` for either implementation
        - Added the env-tied `#!/usr/bin/env python` shabang to allow for direct calling (and accomodation of the **venv** python path)
1. The **c** version is only there to demonstrate the independence of Functional Test from developer implementation, specifically:
    1. There `hello` binary was manually compiled and included in the git repo for demonstration
    1. No linting, building, or unit testing was accomplished in the pipeline
